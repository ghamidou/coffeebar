import os
import csv
import string
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
fname = "/Users/gilleshamidouch/Downloads/GertDeGeyterUNAMUR-examunamur2018-6c3bc6b516c4/Data/Coffeebar_2013-2017.csv"
file = open(fname, "r")
try:
    #List creation from CSV file
    reader = csv.reader(file)
    list_1 = []
    for row in reader:
        list_1.append(row)

finally:
    file.close()


#List with one element by column
list_2 = []


for i in list_1:
    x = i[0]
    list_2.append(x.split(";"))



#Delete first line
del list_2[0]
#print list_2[0:10]

#List creation for counting
list_ID = []
list_dates = []
list_drink = []
list_food = []
list_hours1= []
list_hours2= []
already_in_drinks = []
already_in_food = []
already_in_custo = []


for i in list_2:
        if i[2] not in already_in_drinks:
            already_in_drinks.append (i[2])
        if i[3] not in already_in_food:
            already_in_food.append (i[3])


def colonne_split (column):
    '''
    Split column from excel and create list from this split
    :param column: column like 0 for date, 1 for ID, 2 for drink, 3 for food (int)
    :return: list_dates: list with all the date of the csv file (list)
    :return: list_drink: list with all the drink of the csv file (list)
    :return: list_food: list with all the food of the csv file (list)
    :return: list_ID: list with all the ID of the csv file (list)
    '''
    for i in list_2:
        if column == 0:
            list_dates.append(i[column])
        elif column == 1:
            list_ID.append (i[column])
        elif column == 2:
            list_drink.append (i[column])
        elif column == 3:
             list_food.append (i[column])
        else:
            print ("Veuillez entrez une colonne correcte (De 0 a 3)")
    return list_dates, list_drink, list_food, list_ID

colonne_split(0)
colonne_split(1)
colonne_split(2)
colonne_split(3)

i=0
for element in list_dates:
    word = list_dates[i].split()
    list_hours1.append(word)
    i+=1

j=0
for element in list_hours1:
    word = list_hours1[j][1]
    list_hours2.append(word)
    j+=1

list_ID.sort()
temp = 0
count = 0
for element in list_ID:
    if temp != element:
        count += 1
    temp = element




def average_drink(hour, typeDrink):
    '''
    Give the probability to order a type of drink at a certain time
    :param hour:
    :param typeDrink:
    :return: drink, avg_drink, hour
    '''
    drink = 0
    z=0
    total_drink = 0

    for ele in list_hours2:
        if(ele == hour):
            total_drink +=1
            if(list_drink[z] == typeDrink):
                drink +=1
        z+=1
    avg_drink = 100 * (drink / total_drink)
    print('La probabilite de commander un ' + typeDrink + ' a ' + hour + ' est de: ' + str(avg_drink) + ' %')

    return drink, avg_drink, hour;

def average_food(hour, typeFood):
    '''
    Give the probability to order a type of food at a certain time
    :param hour: hour linked with the probability (str)
    :param typeFood: food linked with the probability (str)
    :return: food: food linked with the probability (str)
    :return: avg_food: probability at a certain time for a type of food (float)
    :return: hour: hour linked with the probability (str)
    '''
    food = 0
    z=0
    total_food = 0

    for ele in list_hours2:
        if(ele == hour):
            total_food +=1
            if(list_food[z] == typeFood):
                food +=1
        z+=1
    avg_food = 100 * (food / total_food)
    print('La probabilite de commander un ' + typeFood + ' a ' + hour + ' est de: ' + str(avg_food) + ' %')

    return food, avg_food, hour;

def average2_food(hour):
    '''
    Give the probabilities to order each type of food at a certain time
    :param hour: hour linked with the probability (str)
    :return: probaall_food: list of the probabilities for each food (list)
    '''
    muffin = 0
    pie = 0
    cookie = 0
    sandwich = 0
    food = 0
    z = 0
    total_food = 0


    for ele in list_hours2:
        if (ele == hour):
            if (list_food[z] == 'muffin'):
                muffin += 1
                total_food += 1
            elif (list_food[z] == 'cookie'):
                cookie += 1
                total_food += 1
            elif (list_food[z] == 'sandwich'):
                sandwich += 1
                total_food += 1
            elif (list_food[z] == 'pie'):
                pie += 1
                total_food += 1
        z += 1
    probacookie= 100*cookie/max(total_food,1)
    probamuffin= 100*muffin/max(total_food,1)
    probapie= 100*pie/max(total_food,1)
    probasandwich= 100*sandwich/max(total_food,1)
    probaall_food = [probapie, probacookie, probamuffin, probasandwich]

    '''print('La probabilite de commander un cookie: '+ str(probacookie) + ' muffin : ' + str(probamuffin) + ' pie : ' + str(probapie) + ' sandwich : ' + str(probasandwich))'''
    return probaall_food


def average2_drink(hour):
    '''
        Give the probabilities to order each type of drink at a certain time
        :param hour: hour linked with the probability (str)
        :return: probaall_drink: list of the probabilities for each drink (list)
    '''
    soda = 0
    tea = 0
    coffee = 0
    water = 0
    frappucino = 0
    milkshake = 0
    drink = 0
    z = 0
    total_drink = 0

    for ele in list_hours2:
        if (ele == hour):
            if (list_drink[z] == 'soda'):
                soda += 1
                total_drink += 1
            elif (list_drink[z] == 'tea'):
                tea += 1
                total_drink += 1
            elif (list_drink[z] == 'coffee'):
                coffee += 1
                total_drink += 1
            elif (list_drink[z] == 'water'):
                water += 1
                total_drink += 1
            elif (list_drink[z] == 'frappucino'):
                frappucino += 1
                total_drink += 1
            elif (list_drink[z] == 'milkshake'):
                milkshake += 1
                total_drink += 1
        z += 1

        probasoda = 100 * soda / max(total_drink, 1)
        probatea = 100*tea/max(total_drink,1)
        probacoffee = 100 * coffee / max(total_drink,1)
        probawater = 100*water/max(total_drink,1)
        probafrap = 100*frappucino/max(total_drink,1)
        probamilk = 100*milkshake/max(total_drink,1)
        probaall_drink = [probasoda,probatea,probacoffee, probawater, probafrap, probamilk]


    '''print('La probabilite de commander un soda:'+ str (probasoda) + ' \n\ttea:' + str (probatea) + ' \n\tcoffee:' + str (probacoffee) + ' \n\twater:'+ str (probawater) + ' \n\tfrappucino:' + str (probafrap) +' \n\tmilkshake:' +str(probamilk) )'''
    return probaall_drink

#Delete quotation marks in order to print answers
#Answer Question1
'''print (already_in_drinks,
       already_in_food,
       count)'''
#Answer Question 2 on another file (CoffeeBar_plots_exploratory)
#Answer Question 3
'''for hours in list_hours2:
    print(average2_food(hours))
    print(average2_drink(hours)) or delete quotation marks at the lines 159 and 205'''





