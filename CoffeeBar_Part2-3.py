from random import *

import CoffeeBar_Part1

#Classes'creation
globalListHipster = []
globalListRegular = []
globalListOneTime = []

class Customer:
    def __init__(self, food, drinks):
        '''
        Create methods for Customer
        :param food: str
        :param drinks: str
        '''
        self.food = food
        self.drinks = drinks


class One(Customer):
    def __init__(self, food, drinks):
        '''
            Create methods for New Client
            :param food: str
            :param drinks: str
        '''
        Customer.__init__(self, food, drinks)
        self.budget = 100
        self.nbr_visites= 1


class Regular_1(One):
    def __init__(self, food, drinks):
        '''
        Create methods for New Client out of trip advisor
        :param food: str
        :param drinks: str
        '''
        One.__init__(self, food, drinks)


class Tripadvisor(One):
    def __init__(self, food, drinks, tip):
        '''
            Create methods for New Client out of trip advisor
            :param: tip: int
            :param food: str
            :param drinks: str
        '''
        One.__init__(self, food, drinks)
        self.tip = randint(0,10)

class Many(Customer):
    def __init__(self, food, drinks, nbr_visites):
        '''
        Create Customer who come minimum 2times
        :param food: str
        :param drinks: str
        :param nbr_visites: int
        '''
        Customer.__init__(self, food, drinks)
        self.nbr_visites = nbr_visites

class Regular_Many (Many):

    def __init__(self, food, drinks, nbr_visites):
        '''
        Create Customer who come minimum 2times and not Hipster
        :param food: str
        :param drinks: str
        :param nbr_visites: int
        '''
        Many.__init__(self, food, drinks, nbr_visites)
        self.budget = 250

class Hipster(Many):
    def __init__(self, food, drinks, nbr_visites):
        '''
            Create Customer who come minimum 2times and not Hipster
            :param food: str
            :param drinks: str
            :param nbr_visites: int
        '''
        Many.__init__(self, food, drinks, nbr_visites)
        self.budget= 500


#People's creation in the different classes

for i in range (0,333):
    ran = randint(0,100)
    rand2 = randint(0,len(CoffeeBar_Part1.list_hours2))
    listProbFood = CoffeeBar_Part1.average2_food(CoffeeBar_Part1.list_hours2[rand2])#random or in order(i)
    rand3 = randint(0, 100)
    listProbDrink = CoffeeBar_Part1.average2_drink(CoffeeBar_Part1.list_hours2[rand2])#random or in order(i)
    if(ran < listProbFood[0]):
        if(rand3 < listProbDrink[0]):
            globalListHipster.append(Hipster("pie", "soda", 100))
        elif (rand3 < listProbDrink[1] + listProbDrink[0]):
            globalListHipster.append(Hipster("pie", "tea", 100))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("pie", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("pie", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("pie", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("pie", "milk", 100))
        else:
            globalListHipster.append(Hipster("pie", "", 100))

    elif (ran < listProbFood[1] + listProbFood[0] ):
        if (rand3 < listProbDrink[0]):
            globalListHipster.append(Hipster("cookie", "soda", 100))
        elif (rand3 < listProbDrink[1] + listProbDrink[0]):
            globalListHipster.append(Hipster("cookie", "tea", 100))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("cookie", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("cookie", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("cookie", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("cookie", "milk", 100))
        else:
            globalListHipster.append(Hipster("cookie", "", 100))

    elif (ran < listProbFood[2] + listProbFood[1] + listProbFood[0]):
        if (rand3 < listProbDrink[0]):
            globalListHipster.append(Hipster("muffin", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("muffin", "tea", 100))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("muffin", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("muffin", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("muffin", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("muffin", "milk", 100))
        else:
            globalListHipster.append(Hipster("muffin", "", 100))

    elif (ran < listProbFood[3] + listProbFood[2] + listProbFood[1] + listProbFood[0]):

        if (rand3 < listProbDrink[0]):
            globalListHipster.append(Hipster("sandwich", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("sandwich", "tea", 100))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("sandwich", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("sandwich", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("sandwich", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("sandwich", "milk", 100))
        else:
            globalListHipster.append(Hipster("sandwich", "", 100))
    else:
        if (rand3 < listProbDrink[0]):
            globalListHipster.append(Hipster("", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("", "tea", 100))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListHipster.append(Hipster("", "milk", 100))
        else:
            globalListHipster.append(Hipster("", "", 100))
for i in range (0,667):
    ran = randint(0,100)
    rand2 = randint(0,len(CoffeeBar_Part1.list_hours2))
    listProbFood = CoffeeBar_Part1.average2_food(CoffeeBar_Part1.list_hours2[rand2])#random or in order(i)
    rand3 = randint(0, 100)
    listProbDrink = CoffeeBar_Part1.average2_drink(CoffeeBar_Part1.list_hours2[rand2])#random or in order(i)
    if (ran < listProbFood[0]):
        if (rand3 < listProbDrink[0]):
            globalListRegular.append(Regular_Many("pie", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("pie", "tea", 100))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("pie", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0] ):
            globalListRegular.append(Regular_Many("pie", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("pie", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("pie", "milk", 100))
        else:
            globalListRegular.append(Regular_Many("pie", "", 100))

    elif (ran < listProbFood[1] + listProbFood[0]):
        if (rand3 < listProbDrink[0]):
            globalListRegular.append(Regular_Many("cookie", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("cookie", "tea", 100))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("cookie", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("cookie", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("cookie", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("cookie", "milk", 100))
        else:
            globalListRegular.append(Regular_Many("cookie", "", 100))

    elif (ran < listProbFood[2] + listProbFood[1] + listProbFood[0]):
        if (rand3 < listProbDrink[0]):
            globalListRegular.append(Regular_Many("muffin", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("muffin", "tea", 100))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("muffin", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("muffin", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("muffin", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("muffin", "milk", 100))
        else:
            globalListRegular.append(Regular_Many("muffin", "", 100))

    elif (ran < listProbFood[3] + listProbFood[2] + listProbFood[1] + listProbFood[0]):

        if (rand3 < listProbDrink[0]):
            globalListRegular.append(Regular_Many("sandwich", "soda", 100))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("sandwich", "tea", 100))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("sandwich", "coffee", 100))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("sandwich", "water", 100))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("sandwich", "frappucino", 100))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListRegular.append(Regular_Many("sandwich", "milk", 100))
        else:
            globalListRegular.append(Regular_Many("sandwich", "", 100))
    else:
        globalListRegular.append(Regular_Many("", "", 100))

for i in range (0,1000):
    ran = randint(0,100)
    rand2 = randint(0,len(CoffeeBar_Part1.list_hours2))
    listProbFood = CoffeeBar_Part1.average2_food(CoffeeBar_Part1.list_hours2[i]) #random or in order(i)
    rand3 = randint(0, 100)
    listProbDrink = CoffeeBar_Part1.average2_drink(CoffeeBar_Part1.list_hours2[i])#random or in order(i)
    if(ran < listProbFood[0]):
        if(rand3 < listProbDrink[0]):
            globalListOneTime.append(One("pie", "soda"))
        elif (rand3 < listProbDrink[1] + listProbDrink[0]):
            globalListOneTime.append(One("pie", "tea"))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("pie", "coffee"))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("pie", "water"))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("pie", "frappucino"))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("pie", "milk"))
        else:
            globalListOneTime.append(One("pie", ""))

    elif (ran < listProbFood[1] + listProbFood[0] ):
        if (rand3 < listProbDrink[0]):
            globalListOneTime.append(One("cookie", "soda"))
        elif (rand3 < listProbDrink[1] + listProbDrink[0]):
            globalListOneTime.append(One("cookie", "tea"))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("cookie", "coffee"))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("cookie", "water"))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("cookie", "frappucino"))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("cookie", "milk"))
        else:
            globalListOneTime.append(One("cookie", ""))

    elif (ran < listProbFood[2] + listProbFood[1] + listProbFood[0]):
        if (rand3 < listProbDrink[0]):
            globalListOneTime.append(One("muffin", "soda"))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("muffin", "tea"))
        elif (rand3 < listProbDrink[2]+ listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("muffin", "coffee"))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("muffin", "water"))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("muffin", "frappucino"))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("muffin", "milk"))
        else:
            globalListOneTime.append(One("muffin", ""))

    elif (ran < listProbFood[3] + listProbFood[2] + listProbFood[1] + listProbFood[0]):

        if (rand3 < listProbDrink[0]):
            globalListOneTime.append(One("sandwich", "soda"))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("sandwich", "tea"))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("sandwich", "coffee"))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("sandwich", "water"))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("sandwich", "frappucino"))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("sandwich", "milk"))
        else:
            globalListOneTime.append(One("sandwich", ""))
    else:
        if (rand3 < listProbDrink[0]):
            globalListOneTime.append(One("", "soda"))
        elif (rand3 < listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("", "tea"))
        elif (rand3 < listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("", "coffee"))
        elif (rand3 < listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("", "water"))
        elif (rand3 < listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("", "frappucino"))
        elif (rand3 < listProbDrink[5] + listProbDrink[4] + listProbDrink[3] + listProbDrink[2] + listProbDrink[1]+ listProbDrink[0]):
            globalListOneTime.append(One("", "milk"))
        else:
            globalListOneTime.append(One("", ""))
#CSV file creation
listCSV = []

for i in range(0,1000):
    if i<800:
        randomtips = randint(0, 100)
        if randomtips >= 92:
            tips = randint(0,10)
        else:
            tips = 0
        money_drink = 0
        money_food = 0
        if globalListOneTime[i].drinks == 'tea':
            money_drink = 3
        elif globalListOneTime[i].drinks == 'soda':
            money_drink = 3
        elif globalListOneTime[i].drinks == 'milkshake':
            money_drink = 5
        elif globalListOneTime[i].drinks == 'water':
            money_drink = 2
        elif globalListOneTime[i].drinks == 'frappucino':
            money_drink = 4
        elif globalListOneTime[i].drinks == 'coffee':
            money_drink = 3

        if globalListOneTime[i].food == 'muffin':
            money_food = 3
        elif globalListOneTime[i].food == 'cookie':
            money_food = 2
        elif globalListOneTime[i].food == 'sandwich':
            money_food = 5
        elif globalListOneTime[i].food == 'pie':
            money_food = 3

        totalamount = tips + money_drink + money_food
        budget = 100 - money_food - money_drink

        listCSV.append([CoffeeBar_Part1.list_dates[i], i , globalListOneTime[i].drinks, globalListOneTime[i].food, tips, totalamount, budget])

    else:
        tips = 0
        money_drink = 0
        money_food = 0
        if globalListOneTime[i].drinks == 'tea':
            money_drink = 3
        elif globalListOneTime[i].drinks == 'soda':
            money_drink = 3
        elif globalListOneTime[i].drinks == 'milkshake':
            money_drink = 5
        elif globalListOneTime[i].drinks == 'water':
            money_drink = 2
        elif globalListOneTime[i].drinks == 'frappucino':
            money_drink = 4
        elif globalListOneTime[i].drinks == 'coffee':
            money_drink = 3

        if globalListOneTime[i].food == 'muffin':
            money_food = 3
        elif globalListOneTime[i].food == 'cookie':
            money_food = 2
        elif globalListOneTime[i].food == 'sandwich':
            money_food = 5
        elif globalListOneTime[i].food == 'pie':
            money_food = 3

        totalamount = money_drink + money_food

        id2 = randint(0,800)
        randomBudget = randint(0,200)
        if randomBudget <= 60:
            budget = 500 - money_food - money_drink
        else:
            budget = 250 - money_food - money_drink
        listCSV.append([CoffeeBar_Part1.list_dates[i], id2, globalListOneTime[i].drinks, globalListOneTime[i].food, tips,totalamount, budget ])


import csv


with open("client.csv","w+") as my_csv:
    csvWriter = csv.writer(my_csv,delimiter=',')
    csvWriter.writerows(listCSV)


